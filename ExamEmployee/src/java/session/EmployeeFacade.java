/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Employee;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author this PC
 */
@Stateless
public class EmployeeFacade extends AbstractFacade<Employee> implements EmployeeFacadeLocal {

    @PersistenceContext(unitName = "ExamEmployeePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EmployeeFacade() {
        super(Employee.class);
    }

    @Override
    public boolean CreateEmployee(Employee employee) {
        int ID = employee.getId();
        String Fullname = employee.getFullname();
        Date Birthday = employee.getBirthday();
        String Address = employee.getAddress();
        String Position = employee.getPosition();
        String Department = employee.getDepartment();
        Query query = em.createNamedQuery("insert into Employee(id, fullname, birthday, address, position, department) values (?,?,?,?,?,?)" );
        
        query.setParameter(1, ID);
        query.setParameter(2, Fullname);
        query.setParameter(3, Birthday);
        query.setParameter(4, Address);
        query.setParameter(5, Position);
        query.setParameter(6, Department);
        
        int row = query.executeUpdate();
        return row!= 0;
    }

    @Override
    public List<Employee> getAll() {
        Query query = em.createNamedQuery("Employee.findAll");
        List<Employee> employees = query.getResultList();
        return employees;
    }


    
}
