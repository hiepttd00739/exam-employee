<%-- 
    Document   : employee
    Created on : Jan 8, 2020, 8:46:23 AM
    Author     : this PC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div style="border-radius: 5px;width: 300px;margin: 50px;background: rgb(236, 236, 236);padding: 50px;">
            <form action="EmployeeServlet" method="post">
                ID:<br><input class="input" type="text" name="id"value="${employee.id}"><br>
                Full Name<br><input class="input" type="text" name="fullname" value="${employee.fullname}"><br>
                Birthday<br><input class="input" type="text" name="birthday"value="${employee.birthday}"><br>
                Address<br><input class="input" type="text" name="address"value="${employee.address}"><br>
                Position<br><input class="input" type="text" name="position"value="${employee.position}"><br>
                Department<br><input class="input" type="text" name="department"value="${employee.department}"><br>
                <input style="width: 100px;height: 24px;margin-top: 15px;" type="submit" name = "action" value="Submit">
                <input style="width: 100px;height: 24px;margin-top: 15px;"type="reset" name = "action" value="Reset">
            </form>
        </div>
    </body>
</html>
